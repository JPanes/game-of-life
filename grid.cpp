/**
 * Implements a class representing a 2d grid of cells.
 *      - New cells are initialized to Cell::DEAD.
 *      - Grids can be resized while retaining their contents in the remaining area.
 *      - Grids can be rotated, cropped, and merged together.
 *      - Grids can return counts of the alive and dead cells.
 *      - Grids can be serialized directly to an ascii std::ostream.
 *
 * You are encouraged to use STL container types as an underlying storage mechanism for the grid cells.
 *
 * @author 961006
 * @date March, 2020
 */
#include "grid.h"
#include <vector>
#include <exception>
#include <iostream>
// Include the minimal number of headers needed to support your implementation.
// #include ...

/**
 * Grid::Grid()
 *
 * Construct an empty grid of size 0x0.
 * Can be implemented by calling Grid::Grid(square_size) constructor.
 *
 * @example
 *
 *      // Make a 0x0 empty grid
 *      Grid grid;
 *
 */
Grid::Grid() {
    this->width = 0;
    this->height = 0;

    std::vector<Cell> grid;
    this->grid = grid;

}

/**
 * Grid::Grid(square_size)
 *
 * Construct a grid with the desired size filled with dead cells.
 * Single value constructors should be marked "explicit" to prevent them
 * being used to implicitly cast ints to grids on construction.
 *
 * Can be implemented by calling Grid::Grid(width, height) constructor.
 *
 * @example
 *
 *      // Make a 16x16 grid
 *      Grid x(16);
 *
 *      // Also make a 16x16 grid
 *      Grid y = Grid(16);
 *
 *      // This should be a compiler error! We want to prevent this from being allowed.
 *      Grid z = 16;
 *
 * @param square_size
 *      The edge size to use for the width and height of the grid.
 */
Grid::Grid(int square_size) {
    this->width = square_size;
    this->height = square_size;
    
    //Initialise the grid with dead cells
    std::vector<Cell> grid (square_size*square_size, Cell::DEAD);
    this->grid = grid;
    
}

/**
 * Grid::Grid(width, height)
 *
 * Construct a grid with the desired size filled with dead cells.
 *
 * @example
 *
 *      // Make a 16x9 grid
 *      Grid grid(16, 9);
 *
 * @param width
 *      The width of the grid.
 *
 * @param height
 *      The height of the grid.
 */
Grid::Grid(int width, int height) {
    this->width = width;
    this->height = height;
    
    //Initialise the grid with dead cells
    std::vector<Cell> grid (width*height, Cell::DEAD);
    this->grid = grid;
    
}

/**
 * Grid::get_width()
 *
 * Gets the current width of the grid.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the width of the grid to the console
 *      std::cout << grid.get_width() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the width of the grid to the console
 *      std::cout << read_only_grid.get_width() << std::endl;
 *
 * @return
 *      The width of the grid.
 */
int Grid::get_width() const {
    return this->width;
}


/**
 * Grid::get_height()
 *
 * Gets the current height of the grid.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the height of the grid to the console
 *      std::cout << grid.get_height() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the height of the grid to the console
 *      std::cout << read_only_grid.get_height() << std::endl;
 *
 * @return
 *      The height of the grid.
 */
int Grid::get_height() const {
    return this->height;
}

/**
 * Grid::get_total_cells()
 *
 * Gets the total number of cells in the grid.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the total number of cells on the grid to the console
 *      std::cout << grid.get_total_cells() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the total number of cells on the grid to the console
 *      std::cout << read_only_grid.get_total_cells() << std::endl;
 *
 * @return
 *      The number of total cells.
 */
int Grid::get_total_cells() const {
    return this->width * this->height;
}

/**
 * Grid::get_alive_cells()
 *
 * Counts how many cells in the grid are alive.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the number of alive cells to the console
 *      std::cout << grid.get_alive_cells() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the number of alive cells to the console
 *      std::cout << read_only_grid.get_alive_cells() << std::endl;
 *
 * @return
 *      The number of alive cells.
 */
int Grid::get_alive_cells() const {
    int alive_cells = 0;
    for(const auto & cell: this->grid) {
        if (cell == Cell::ALIVE) {
            alive_cells++;
        }
    }
    return alive_cells;
}

/**
 * Grid::get_dead_cells()
 *
 * Counts how many cells in the grid are dead.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the number of dead cells to the console
 *      std::cout << grid.get_dead_cells() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the number of dead cells to the console
 *      std::cout << read_only_grid.get_dead_cells() << std::endl;
 *
 * @return
 *      The number of dead cells.
 */
int Grid::get_dead_cells() const {
    int dead_cells = 0;
    for(const auto & cell: this->grid) {
        if (cell == Cell::DEAD) {
            dead_cells++;
        }
    }

    return dead_cells;
}

/**
 * Grid::resize(square_size)
 *
 * Resize the current grid to a new width and height that are equal. The content of the grid
 * should be preserved within the kept region and padded with Grid::DEAD if new cells are added.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Resize the grid to be 8x8
 *      grid.resize(8);
 *
 * @param square_size
 *      The new edge size for both the width and height of the grid.
 */
void Grid::resize(int square_size) {
    //Get the dimensions of the original grid  
    int curr_width = this->width;
    int curr_height = this->height;

    //Update the values
    this->width = square_size;
    this->height = square_size;

    //Create a new grid initialised to contain only dead cells
    std::vector<Cell> newGrid(square_size*square_size, Cell::DEAD);
    
    //Due to the difference in size, it is necessary to keep track of where things should be placed 
    int new_grid_position = 0;
    int curr_grid_position = 0;
    
    //Iterate over the old grid to find any alive cells
    for(int y = 0; y < square_size; y++) {
        for(int x = 0; x < square_size; x++) {
            
            //If within the boundaries of the original grid, transfer value over
            if (curr_height-1 >= y && curr_width-1 >= x) {
                if (this->grid.at(curr_grid_position) == Cell::ALIVE) {
                    newGrid.at(new_grid_position) = this->grid.at(curr_grid_position);
                }
                //Increment position when within the bounds of the original
                curr_grid_position++;
            }
            new_grid_position++;
        }

        //When the grid is smaller than the original, cut out excess
        if (square_size < curr_width) {
            curr_grid_position += curr_width - square_size;
        }

    }
    //Replace original grid with new version
    this->grid = newGrid;

}


/**
 * Grid::resize(width, height)
 *
 * Resize the current grid to a new width and height. The content of the grid
 * should be preserved within the kept region and padded with Grid::DEAD if new cells are added.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Resize the grid to be 2x8
 *      grid.resize(2, 8);
 *
 * @param new_width
 *      The new width for the grid.
 *
 * @param new_height
 *      The new height for the grid.
 */
void Grid::resize(int new_width, int new_height) {
    //Get the dimensions of the original grid  
    int curr_width = this->get_width();
    int curr_height = this->get_height();

    //Update the values
    this->width = new_width;
    this->height = new_height;

    //Create a new grid initialised to contain only dead cells
    std::vector<Cell> newGrid(new_width*new_height, Cell::DEAD);
    
    //Due to the difference in size, it is necessary to keep track of where things should be placed 
    int new_grid_position = 0;
    int curr_grid_position = 0;
    
    //Iterate over the old grid to find any alive cells
    for(int y = 0; y < new_height; y++) {
        for(int x = 0; x < new_width; x++) {
            
            //If within the boundaries of the original grid, transfer value over
            if (curr_height-1 >= y && curr_width-1 >= x) {
                if (this->grid.at(curr_grid_position) == Cell::ALIVE) {
                    newGrid.at(new_grid_position) = this->grid.at(curr_grid_position);
                }
                //Increment position when within the bounds of the original
                curr_grid_position++;
            }
            new_grid_position++;
        }

        //When the grid is smaller than the original, cut out excess
        if (new_width < curr_width) {
            curr_grid_position += curr_width - new_width;
        }

    }
    //Replace original grid with new version
    this->grid = newGrid;

}

/**
 * Grid::get_index(x, y)
 *
 * Private helper function to determine the 1d index of a 2d coordinate.
 * Should not be visible from outside the Grid class.
 * The function should be callable from a constant context.
 *
 * @param x
 *      The x coordinate of the cell.
 *
 * @param y
 *      The y coordinate of the cell.
 *
 * @return
 *      The 1d offset from the start of the data array where the desired cell is located.
 */
int Grid::get_index(int x, int y) const {
    
    return (y * this->get_width()) + x;
}

/**
 * Grid::get(x, y)
 *
 * Returns the value of the cell at the desired coordinate.
 * Specifically this function should return a cell value, not a reference to a cell.
 * The function should be callable from a constant context.
 * Should be implemented by invoking Grid::operator()(x, y).
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Read the cell at coordinate (1, 2)
 *      Cell cell = grid.get(1, 2);
 *
 * @param x
 *      The x coordinate of the cell to update.
 *
 * @param y
 *      The y coordinate of the cell to update.
 *
 * @return
 *      The value of the desired cell. Should only be Grid::ALIVE or Grid::DEAD.
 *
 * @throws
 *      std::exception or sub-class if x,y is not a valid coordinate within the grid.
 */
Cell Grid::get(int x, int y) const {
    return this->operator()(x,y); 
}


/**
 * Grid::set(x, y, value)
 *
 * Overwrites the value at the desired coordinate.
 * Should be implemented by invoking Grid::operator()(x, y).
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Assign to a cell at coordinate (1, 2)
 *      grid.set(1, 2, Cell::ALIVE);
 *
 * @param x
 *      The x coordinate of the cell to update.
 *
 * @param y
 *      The y coordinate of the cell to update.
 *
 * @param value
 *      The value to be written to the selected cell.
 *
 * @throws
 *      std::exception or sub-class if x,y is not a valid coordinate within the grid.
 */
void Grid::set(int x, int y, Cell value) {
    this->operator()(x,y) = value;
}

/**
 * Grid::operator()(x, y)
 *
 * Gets a modifiable reference to the value at the desired coordinate.
 * Should be implemented by invoking Grid::get_index(x, y).
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Get access to read a cell at coordinate (1, 2)
 *      Cell cell = grid(1, 2);
 *
 *      // Directly assign to a cell at coordinate (1, 2)
 *      grid(1, 2) = Cell::ALIVE;
 *
 *      // Extract a reference to an individual cell to avoid calculating it's
 *      // 1d index multiple times if you need to access the cell more than once.
 *      Cell &cell_reference = grid(1, 2);
 *      cell_reference = Cell::DEAD;
 *      cell_reference = Cell::ALIVE;
 *
 * @param x
 *      The x coordinate of the cell to access.
 *
 * @param y
 *      The y coordinate of the cell to access.
 *
 * @return
 *      A modifiable reference to the desired cell.
 *
 * @throws
 *      std::runtime_error or sub-class if x,y is not a valid coordinate within the grid.
 */
Cell& Grid::operator()(int x, int y) {    
    return grid[get_index(x,y)];
}

/**
 * Grid::operator()(x, y)
 *
 * Gets a read-only reference to the value at the desired coordinate.
 * The operator should be callable from a constant context.
 * Should be implemented by invoking Grid::get_index(x, y).
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Constant reference to a grid (does not make a copy)
 *      const Grid &read_only_grid = grid;
 *
 *      // Get access to read a cell at coordinate (1, 2)
 *      Cell cell = read_only_grid(1, 2);
 *
 * @param x
 *      The x coordinate of the cell to access.
 *
 * @param y
 *      The y coordinate of the cell to access.
 *
 * @return
 *      A read-only reference to the desired cell.
 *
 * @throws
 *      std::exception or sub-class if x,y is not a valid coordinate within the grid.
 */
const Cell& Grid::operator()(int x, int y) const {
    return grid[get_index(x,y)];
}

/**
 * Grid::crop(x0, y0, x1, y1)
 *
 * Extract a sub-grid from a Grid.
 * The cropped grid spans the range [x0, x1) by [y0, y1) in the original grid.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid y(4, 4);
 *
 *      // Crop the centre 2x2 in y, trimming a 1 cell border off all sides
 *      Grid x = y.crop(x, 1, 1, 3, 3);
 *
 * @param x0
 *      Left coordinate of the crop window on x-axis.
 *
 * @param y0
 *      Top coordinate of the crop window on y-axis.
 *
 * @param x1
 *      Right coordinate of the crop window on x-axis (1 greater than the largest index).
 *
 * @param y1
 *      Bottom coordinate of the crop window on y-axis (1 greater than the largest index).
 *
 * @return
 *      A new grid of the cropped size containing the values extracted from the original grid.
 *
 * @throws
 *      std::exception or sub-class if x0,y0 or x1,y1 are not valid coordinates within the grid
 *      or if the crop window has a negative size.
 */
Grid Grid::crop(int x0, int y0, int x1, int y1) const {
    Grid cropped_grid;
    //Get the dimensions of the cropped area
    cropped_grid.width = x1 - x0;
    cropped_grid.height = y1 - y0;
    //Reserve the necessary amount of memory
    std::vector<Cell> newGrid;
    
    //Iterate over the area to be cropped
    for(int curr_y_pos = y0; curr_y_pos< y1; curr_y_pos++) {
        //Where on the current 'row' to start and stop
        int startPos = get_index(x0, curr_y_pos);
        int endPos = get_index(x1, curr_y_pos);
        
        //Keep track of 2D x Co-ord for retrieval of values
        int curr_x_pos = x0;
        for(int currPos = startPos; currPos < endPos; currPos++) {
            newGrid.push_back(this->get(curr_x_pos,curr_y_pos));
            curr_x_pos++;
        }
    }
    cropped_grid.grid = newGrid;
    return cropped_grid;
}


/**
 * Grid::merge(other, x0, y0, alive_only = false)
 *
 * Merge two grids together by overlaying the other on the current grid at the desired location.
 * By default merging overwrites all cells within the merge reason to be the value from the other grid.
 *
 * Conditionally if alive_only = true perform the merge such that only alive cells are updated.
 *      - If a cell is originally dead it can be updated to be alive from the merge.
 *      - If a cell is originally alive it cannot be updated to be dead from the merge.
 *
 * @example
 *
 *      // Make two grids
 *      Grid x(2, 2), y(4, 4);
 *
 *      // Overlay x as the upper left 2x2 in y
 *      y.merge(x, 0, 0);
 *
 *      // Overlay x as the bottom right 2x2 in y, reading only alive cells from x
 *      y.merge(x, 2, 2, true);
 *
 * @param other
 *      The other grid to merge into the current grid.
 *
 * @param x0
 *      The x coordinate of where to place the top left corner of the other grid.
 *
 * @param y0
 *      The y coordinate of where to place the top left corner of the other grid.
 *
 * @param alive_only
 *      Optional parameter. If true then merging only sets alive cells to alive but does not explicitly set
 *      dead cells, allowing whatever value was already there to persist. Defaults to false.
 *
 * @throws
 *      std::exception or sub-class if the other grid being placed does not fit within the bounds of the current grid.
 */
void Grid::merge(Grid other, int x0, int y0, bool alive_only) {
    //Calculate endpoint for merge
    int width_end_point;
    int height_end_point;

    //In the event that the grid that is being overlaid would go out of bounds, 
    //only iterate to the end of the current grid
    

    if (other.get_height() +y0 > this->get_height()){
        height_end_point = this->get_height();
    } else {
        height_end_point = other.get_height() + x0;
    }
    
    //Keep track of position in other grid
    int other_y_pos = 0;
    int other_x_pos;
    
    //Iterate over the area to be merged
    for(int curr_y_pos = y0; curr_y_pos< height_end_point; curr_y_pos++) {
        //Where on the current 'row' to start and stop
        int startPos = get_index(x0, curr_y_pos);
        
        if (other.get_width() +x0 > this->get_width()) {
            width_end_point = get_index(this->get_width(), curr_y_pos);
        } else {
            width_end_point = get_index(other.get_width() +x0, curr_y_pos);
        }
        
        //Keep track of 2D x Co-ord for retrieval of values
        int curr_x_pos = x0;
        other_x_pos = 0;

        //When iterating over the current row act differently depending upon alive_only (see above)
        for(int currPos = startPos; currPos < width_end_point; currPos++) {
            if(alive_only && this->get(curr_x_pos,curr_y_pos) != ALIVE) {
                this->set(curr_x_pos, curr_y_pos, other.get(other_x_pos, other_y_pos));
            } else if (alive_only == false) {
                this->set(curr_x_pos, curr_y_pos, other.get(other_x_pos, other_y_pos));
            }
            other_x_pos++;
            curr_x_pos++;
        }
        other_y_pos++;
    }
}

/**
 * Grid::rotate(rotation)
 *
 * Create a copy of the grid that is rotated by a multiple of 90 degrees.
 * The rotation can be any integer, positive, negative, or 0.
 * The function should take the same amount of time to execute for any valid integer input.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a 1x3 grid
 *      Grid x(1,3);
 *
 *      // y is size 3x1
 *      Grid y = x.rotate(1);
 *
 * @param _rotation
 *      An positive or negative integer to rotate by in 90 intervals.
 *
 * @return
 *      Returns a copy of the grid that has been rotated.
 */
Grid Grid::rotate(int rotation) const {

    Grid rotated_grid;
    //Reduce potential rotations down to the 4 unique rotations
    int actual_rotation;

    bool clockwise;

    if (rotation >= 0) {
        clockwise = true;
        actual_rotation = rotation % 4;
    } else {
        clockwise = false;
        actual_rotation = rotation % -4;
    }
    
    //Set the initial values of the new Grid
    rotated_grid.grid = this->grid;
    rotated_grid.width = this->get_width();
    rotated_grid.height = this->get_height();

    if (clockwise) {
        //Loop until it rotation have been completed
        for(int curr_rotation = 0; curr_rotation < actual_rotation; curr_rotation++) {
            std::vector<Cell> rotated_vector;
            //Go over each column from the bottom up, and have that placed in the row
            for(int x = 0; x < rotated_grid.get_width(); x++) {
                for(int y = rotated_grid.get_height()-1; y >= 0; y--) {
                    rotated_vector.push_back(rotated_grid.get(x,y));
                }
            }

            //Update the stored information regarding the current grid state
            rotated_grid.grid = rotated_vector;
            
            int temp = rotated_grid.get_width();
            rotated_grid.width = rotated_grid.get_height();
            rotated_grid.height = temp;
        }
    } else {
        //Loop until it rotation have been completed
        for(int curr_rotation = actual_rotation; curr_rotation < 0; curr_rotation++) {
            std::vector<Cell> rotated_vector;
            //Go over the grid from right to left (going top to bottom as it goes)
            for(int x = rotated_grid.get_width()-1; x >= 0; x--) {
                for(int y = 0; y < rotated_grid.get_height(); y++) {
                    rotated_vector.push_back(rotated_grid.get(x,y));
                }
            }

            //Update the stored information regarding the current grid state
            rotated_grid.grid = rotated_vector;
            int temp = rotated_grid.get_width();
            rotated_grid.width = rotated_grid.get_height();
            rotated_grid.height = temp;
        }
    }

    return rotated_grid;
}


/**
 * operator<<(output_stream, grid)
 *
 * Serializes a grid to an ascii output stream.
 * The grid is printed wrapped in a border of - (dash), | (pipe), and + (plus) characters.
 * Alive cells are shown as # (hash) characters, dead cells with ' ' (space) characters.
 *
 * The function should be callable on a constant Grid.
 *
 * @example
 *
 *      // Make a 3x3 grid with a single alive cell
 *      Grid grid(3);
 *      grid(1, 1) = Cell::ALIVE;
 *
 *      // Print the grid to the console
 *      std::cout << grid << std::endl;
 *
 *      // The grid is printed with a border of + - and |
 *
 *      +---+
 *      |   |
 *      | # |
 *      |   |
 *      +---+
 *
 * @param os
 *      An ascii mode output stream such as std::cout.
 *
 * @param grid
 *      A grid object containing cells to be printed.
 *
 * @return
 *      Returns a reference to the output stream to enable operator chaining.
 */
std::ostream& operator<<(std::ostream &os, const Grid &grid) {
    //Print the border across the top
    os << "+";
    for(int i = 0; i < grid.get_width(); i++) {
        os << "-";
    }
    os << "+\n";
    //Iterate through each row, printing the values
    for (int y = 0; y < grid.get_height(); y++) {
        os << "|";
        
        for(int x = 0; x < grid.get_width(); x++) {
            if(grid.get(x,y) == ALIVE) {
                os << "#";
            } else if(grid.get(x,y) == DEAD) {
                os << " ";
            } else {
                //If this occurs, then an unexpected/ invalid character is present
                os << "E";
            }
        }
        os << "|\n";
    }
    //Print the bottom border
    os << "+";
    for(int i = 0; i < grid.get_width(); i++) {
        os << "-";
    }
    os << "+\n";

    return os;
}
