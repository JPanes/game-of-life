/**
 * Implements a Zoo namespace with methods for constructing Grid objects containing various creatures in the Game of Life.
 *      - Creatures like gliders, light weight spaceships, and r-pentominos can be spawned.
 *          - These creatures are drawn on a Grid the size of their bounding box.
 *
 *      - Grids can be loaded from and saved to an ascii file format.
 *          - Ascii files are composed of:
 *              - A header line containing an integer width and height separated by a space.
 *              - followed by (height) number of lines, each containing (width) number of characters,
 *                terminated by a newline character.
 *              - (space) ' ' is Cell::DEAD, (hash) '#' is Cell::ALIVE.
 *
 *      - Grids can be loaded from and saved to an binary file format.
 *          - Binary files are composed of:
 *              - a 4 byte int representing the grid width
 *              - a 4 byte int representing the grid height
 *              - followed by (width * height) number of individual bits in C-style row/column format,
 *                padded with zero or more 0 bits.
 *              - a 0 bit should be considered Cell::DEAD, a 1 bit should be considered Cell::ALIVE.
 *
 * @author 961006
 * @date March, 2020
 */
#include "zoo.h"
#include "grid.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <bitset>


// Include the minimal number of headers needed to support your implementation.
// #include ...
/**
 * Zoo::glider()
 *
 * Construct a 3x3 grid containing a glider.
 * https://www.conwaylife.com/wiki/Glider
 *
 * @example
 *
 *      // Print a glider in a Grid the size of its bounding box.
 *      std::cout << Zoo::glider() << std::endl;
 *
 *      +---+
 *      | # |
 *      |  #|
 *      |###|
 *      +---+
 *
 * @return
 *      Returns a Grid containing a glider.
 */
Grid Zoo::glider(){
    Grid g(3,3);

    //Place alive cells to form a specific structure of cells
    g.set(1, 0, ALIVE);
    g.set(2, 1, ALIVE);
    g.set(0, 2, ALIVE);
    g.set(1, 2, ALIVE);
    g.set(2, 2, ALIVE);

    return g;
}

/**
 * Zoo::r_pentomino()
 *
 * Construct a 3x3 grid containing an r-pentomino.
 * https://www.conwaylife.com/wiki/R-pentomino
 *
 * @example
 *
 *      // Print an r-pentomino in a Grid the size of its bounding box.
 *      std::cout << Zoo::r_pentomino() << std::endl;
 *
 *      +---+
 *      | ##|
 *      |## |
 *      | # |
 *      +---+
 *
 * @return
 *      Returns a Grid containing a r-pentomino.
 */
Grid Zoo::r_pentomino(){
    Grid g(3,3);

    //Place alive cells to form a specific structure of cells
    g.set(1, 0, ALIVE);
    g.set(2, 0, ALIVE);
    g.set(0, 1, ALIVE);
    g.set(1, 1, ALIVE);
    g.set(1, 2, ALIVE);

    return g;
}

/**
 * Zoo::light_weight_spaceship()
 *
 * Construct a 5x4 grid containing a light weight spaceship.
 * https://www.conwaylife.com/wiki/Lightweight_spaceship
 *
 * @example
 *
 *      // Print a light weight spaceship in a Grid the size of its bounding box.
 *      std::cout << Zoo::light_weight_spaceship() << std::endl;
 *
 *      +-----+
 *      | #  #|
 *      |#    |
 *      |#   #|
 *      |#### |
 *      +-----+
 *
 * @return
 *      Returns a grid containing a light weight spaceship.
 */
Grid Zoo::light_weight_spaceship(){
    Grid g(5, 4);

    //Place alive cells to form a specific structure of cells
    g.set(1, 0, ALIVE);
    g.set(4, 0, ALIVE);

    g.set(0, 1, ALIVE);

    g.set(0, 2, ALIVE);
    g.set(4, 2, ALIVE);

    g.set(0, 3, ALIVE);
    g.set(1, 3, ALIVE);
    g.set(2, 3, ALIVE);
    g.set(3, 3, ALIVE);

    return g;
}

/**
 * Zoo::load_ascii(path)
 *
 * Load an ascii file and parse it as a grid of cells.
 * Should be implemented using std::ifstream.
 *
 * @example
 *
 *      // Load an ascii file from a directory
 *      Grid grid = Zoo::load_ascii("path/to/file.gol");
 *
 * @param path
 *      The std::string path to the file to read in.
 *
 * @return
 *      Returns the parsed grid.
 *
 * @throws
 *      Throws std::runtime_error or sub-class if:
 *          - The file cannot be opened.
 *          - The parsed width or height is not a positive integer.
 *          - Newline characters are not found when expected during parsing.
 *          - The character for a cell is not the ALIVE or DEAD character.
 */
Grid Zoo::load_ascii(std::string path) {
    
    std::ifstream input_file;
    input_file.open(path, std::ios::in);

    if(!input_file.is_open()) {
        //Can't open/find file at current location, try alternatives
        if(path.substr(0,3) == "../") {
            //Try without relative path, to see if the file is here
            //3 is used in order to skip "../"
            path = path.substr(3, path.length()-3);
        }else {
            //if not trying a relative path, give it a go
            path = "../" + path;
        }
        //Try again
        input_file.open(path, std::ios::in);

        if(!input_file.is_open()) {
            //File can't be found despite alternatives, so stop
            return 1;
        }
    }
 
    std::string line;
    getline(input_file, line);
    int width;
    int height;

    std::istringstream curr_line(line);
    //Try and get the dimensions from the grid 
    //dimensions from the first line
    if(curr_line >> width >> height) {
        curr_line >> width >> height;
    } else if (curr_line >> width){
        //In the event that only one number is given
        curr_line >> width;
        curr_line >> height;
    } else {
        //No dimensional values given at start of file
        //throw "Invalid argument - no dimensions given in the file to describe grid structure";
    }

    Grid g(width, height);
    //Used for:
    // 1. keeping track of where we are in the grid
    // 2. making sure we remain within the bounds of the 
    //    grid so no one can pull a sneaky on us
    int curr_y = 0;
    bool error = false;
    
    while(std::getline(input_file, line) && curr_y < height) {
        int curr_x = 0;

        //Iterate through the current line
        while(curr_x < width) {
            if(line[curr_x]) {
                
                //Look through the line and assign values
                //based upon what is comes across
                if(line[curr_x] == ' ') {
                    g.set(curr_x, curr_y, DEAD);
                }else if(line[curr_x] == '#') {
                    g.set(curr_x, curr_y, ALIVE);
                }else {
                    //An unexpected character exists within the grid area
                    //throw "Invalid argument - an unexpected character exists within the grid area";
                }
                curr_x++;
            } else {
                error = true;
                break;
            }
        }
        //In this case something has gone very wrong
        if(error) {
            std::cout << line << std::endl;
            std::cout << curr_x << ", " << curr_y << std::endl;
            break;
        }
        curr_y++;
    }
    if(error == false) {
        return g;
    } else {
        return 1;
    }
}

/**
 * Zoo::save_ascii(path, grid)
 *
 * Save a grid as an ascii .gol file according to the specified file format.
 * Should be implemented using std::ofstream.
 *
 * @example
 *
 *      // Make an 8x8 grid
 *      Grid grid(8);
 *
 *      // Save a grid to an ascii file in a directory
 *      try {
 *          Zoo::save_ascii("path/to/file.gol", grid);
 *      }
 *      catch (const std::exception &ex) {
 *          std::cerr << ex.what() << std::endl;
 *      }
 *
 * @param path
 *      The std::string path to the file to write to.
 *
 * @param grid
 *      The grid to be written out to file.
 *
 * @throws
 *      Throws std::runtime_error or sub-class if the file cannot be opened.
 */
void Zoo::save_ascii(std::string path, Grid grid) {
    std::ofstream output_file;
    output_file.open(path, std::ios::out);
    //Check if file exists
    if(!output_file.is_open()) {
        //Check if file exists removing ../ if present
        //Can't open/find file at current location, try alternatives
        if(path.substr(0,3) == "../") {
            //Try without relative path, to see if the file is here
            //3 is used in order to skip "../"
            path = path.substr(3, path.length()-3);
        }else {
            //if not trying a relative path, give it a go
            path = "../" + path;
        }
        //Try again
        output_file.open(path, std::ios::out);

        if(!output_file.is_open()) {
            //File can't be found despite alternatives, so stop
            exit(1);
        }
    }

    output_file << grid.get_width() << " " << grid.get_height() << "\n";
    //Iterate over the grid, passing the current value to the file
    for(int y = 0; y < grid.get_height(); y++) {
        //Use an intermediate variable to reduce the amount of times the file
        //needs to be accessed
        std::stringstream curr_row;
        for(int x = 0; x < grid.get_width(); x++) {
            if(grid.get(x, y) == ALIVE) {
                curr_row << "#";
            } else if(grid.get(x, y) == DEAD) {
                curr_row << " ";
            }
        }
        curr_row << "\n";
        output_file << curr_row.str();
    }
    output_file.close();
}

/**
 * Zoo::load_binary(path)
 *
 * Load a binary file and parse it as a grid of cells.
 * Should be implemented using std::ifstream.
 *
 * @example
 *
 *      // Load an binary file from a directory
 *      Grid grid = Zoo::load_binary("path/to/file.bgol");
 *
 * @param path
 *      The std::string path to the file to read in.
 *
 * @return
 *      Returns the parsed grid.
 *
 * @throws
 *      Throws std::runtime_error or sub-class if:
 *          - The file cannot be opened.
 *          - The file ends unexpectedly.
 */
Grid Zoo::load_binary(std::string path) {
    std::ifstream input_file;
    input_file.open(path, std::ios::binary);

    if(!input_file.is_open()) {
        //Can't open/find file at current location, try alternatives
        if(path.substr(0,3) == "../") {
            //Try without relative path, to see if the file is here
            //3 is used in order to skip "../"
            path = path.substr(3, path.length()-3);
        }else {
            //if not trying a relative path, give it a go
            path = "../" + path;
        }
        //Try again
        input_file.open(path, std::ios::in);

        if(!input_file.is_open()) {
            //File can't be found despite alternatives, so stop
            return 1;
        }
    }
    
    //Used to store the 4 byte binary values
    int width(4);
    int height(4);
    //Read in the dimensions from the binary file
    input_file.read((char*)&width, sizeof(int));
    input_file.read((char*)&height, sizeof(int));    
    
    Grid new_grid(width, height);

    //Get information necessary to extract the bits from the file
    const int remaining_bits = input_file.tellg();

    std::vector <char> memblock(0);
    
    input_file.read (&memblock[0], remaining_bits);

    char* line = new char[remaining_bits];
    input_file.read(line, remaining_bits);

    //std::bitset<64> bitLine(line);

    input_file.close();

    return new_grid;
}

/**
 * Zoo::save_binary(path, grid)
 *
 * Save a grid as an binary .bgol file according to the specified file format.
 * Should be implemented using std::ofstream.
 *
 * @example
 *
 *      // Make an 8x8 grid
 *      Grid grid(8);
 *
 *      // Save a grid to an binary file in a directory
 *      try {
 *          Zoo::save_binary("path/to/file.bgol", grid);
 *      }
 *      catch (const std::exception &ex) {
 *          std::cerr << ex.what() << std::endl;
 *      }
 *
 * @param path
 *      The std::string path to the file to write to.
 *
 * @param grid
 *      The grid to be written out to file.
 *
 * @throws
 *      Throws std::runtime_error or sub-class if the file cannot be opened.
 */

