/**
 * Declares a class representing a 2d grid of cells.
 * Rich documentation for the api and behaviour the Grid class can be found in grid.cpp.
 *
 * The test suites provide granular BDD style (Behaviour Driven Development) test cases
 * which will help further understand the specification you need to code to.
 *
 * @author 961006
 * @date March, 2020
 */
#pragma once
#include <vector>
#include <iostream>

// Add the minimal number of includes you need in order to declare the class.
// #include ...

/**
 * A Cell is a char limited to two named values for Cell::DEAD and Cell::ALIVE.
 */
enum Cell : char {
    DEAD  = ' ',
    ALIVE = '#'
};

/**
 * Declare the structure of the Grid class for representing a 2d grid of cells.
 */
class Grid {
    private:
        int width, height;
        std::vector<Cell> grid;
    public:
        Grid();
        Grid(int square_size);
        Grid(int width, int height);

        Cell& operator()(int x, int y);
        //Read-only version
        const Cell& operator()(int x, int y) const;
        
        friend std::ostream& operator<<(std::ostream &os, const Grid &grid);

        virtual int get_width() const;
        virtual int get_height() const;
        virtual int get_total_cells() const;

        virtual int get_alive_cells() const;
        virtual int get_dead_cells() const;

        virtual void resize(int square_size);
        virtual void resize(int new_width, int new_height);

        virtual int get_index(int x, int y) const;
        virtual Cell get(int x, int y) const;
        virtual void set(int x, int y, Cell value);
        
        virtual Grid crop(int x0, int y0, int x1, int y1) const;

        virtual void merge(Grid other, int x0, int y0, bool alive_only = false);

        virtual Grid rotate(int rotation) const;
};
